import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { LoginHomePageRoutingModule } from './login-home-routing.module';

import { LoginHomePage } from './login-home.page';
import { LoginPageModule } from '../login/login.module';
import { SignUpPageModule } from '../sign-up/sign-up.module';
import { ForgotPageModule } from '../forgot/forgot.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    LoginHomePageRoutingModule,
    LoginPageModule,
    SignUpPageModule,
    ForgotPageModule
  ],
  declarations: [LoginHomePage]
})
export class LoginHomePageModule {}
