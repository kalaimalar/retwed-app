import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { LoginPage } from '../login/login.page';
import { SignUpPage } from '../sign-up/sign-up.page';
import { ForgotPage } from '../forgot/forgot.page';

@Component({
  selector: 'app-login-home',
  templateUrl: './login-home.page.html',
  styleUrls: ['./login-home.page.scss'],
})
export class LoginHomePage implements OnInit {

  showContent = true;

  constructor(public modalCtrl: ModalController) { }

  ngOnInit() {
  }


  async presentLoginInModal() {
    const modal = await this.modalCtrl.create({
      component: LoginPage,
      mode: 'ios'
    });
    this.showContent = false;
    modal.onDidDismiss().then((res) => {
      this.showContent = true;
      console.log('res', res);
      if (res && res.data && res.data.goToSignUp == true) {
        this.showContent = false;
        this.presentSignUpModal();
      }
      if (res && res.data && res.data.goToForgot == true) {
        this.showContent = false;
        this.presentForgotModal();
      }
    })
    return await modal.present().catch(() => {
      this.showContent = true;
    });
  }
  async presentSignUpModal() {
    const modal = await this.modalCtrl.create({
      component: SignUpPage,
      mode: 'ios'
    });
    this.showContent = false;
    modal.onDidDismiss().then((res) => {
      this.showContent = true;
      if (res && res.data && res.data.goToLogin == true) {
        this.showContent = false;
        this.presentLoginInModal();
      }
    })
    return await modal.present().catch(() => {
      this.showContent = true;
    });
  }
  async presentForgotModal() {
    const modal = await this.modalCtrl.create({
      component: ForgotPage,
      mode: 'ios'
    });
    this.showContent = false;
    modal.onDidDismiss().then((res) => {
      this.showContent = true;
      if (res && res.data && res.data.goToSignUp == true) {
        this.showContent = false;
        this.presentSignUpModal();
      }
    })
    return await modal.present().catch(() => {
      this.showContent = true;
    });
  }


}
