import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';

@Component({
  selector: 'app-forgot',
  templateUrl: './forgot.page.html',
  styleUrls: ['./forgot.page.scss'],
})
export class ForgotPage implements OnInit {

  constructor(
    public modalCtrl: ModalController
  ) { }

  ngOnInit() {
  }

  goToSignUp() {
    this.modalCtrl.dismiss({
      'goToSignUp': true,
      'goToForgot': false,
    });
  }

  
  dismiss() {
    console.log('dismiss clicked');
    this.modalCtrl.dismiss({
      'goToForgot': false,
      'goToSignUp': false
    });
  }
}
