import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { LoginPage } from '../login/login.page';

@Component({
  selector: 'app-sign-up',
  templateUrl: './sign-up.page.html',
  styleUrls: ['./sign-up.page.scss'],
})
export class SignUpPage implements OnInit {

  constructor(public modalCtrl: ModalController) { }

  ngOnInit() {
  }

  dismiss() {
    console.log('dismiss clicked');
    this.modalCtrl.dismiss({
      'goToLogin': false
    });
  }

  signUp() {
    console.log('signup clicked');
  }


  goToLogin() {
    this.modalCtrl.dismiss({
      'goToLogin': true
    });
  }

  logIn() {
    this.dismiss();
    console.log('login Clicked');
  }

}
