import { Component, OnInit } from '@angular/core';
import { ModalController, NavController } from '@ionic/angular';
import { LoginHomePage } from '../login-home/login-home.page';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {

  constructor(
    public modalCtrl: ModalController,
    public navCtrl: NavController
  ) { }

  ngOnInit() {
  }


  goToSignUp() {
    this.modalCtrl.dismiss({
      'goToSignUp': true,
      'goToForgot': false,
    });
  }

  goToForgot() {
    this.modalCtrl.dismiss({
      'goToForgot': true,
      'goToSignUp': false
    });
  }

  dismiss() {
    console.log('dismiss clicked');
    this.modalCtrl.dismiss({
      'goToForgot': false,
      'goToSignUp': false
    });
  }

  goToGetStarted() {
    this.dismiss();
    this.navCtrl.navigateRoot('/getstarted', { animationDirection: 'forward', animated: true })
  }
}
