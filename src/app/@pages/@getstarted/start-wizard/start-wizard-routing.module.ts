import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { StartWizardPage } from './start-wizard.page';

const routes: Routes = [
  {
    path: '',
    component: StartWizardPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class StartWizardPageRoutingModule {}
