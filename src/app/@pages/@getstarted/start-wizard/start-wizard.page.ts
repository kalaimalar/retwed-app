import { Component, OnInit, ViewChild } from '@angular/core';
import { IonSlides } from '@ionic/angular';
import { DatePicker } from '@ionic-native/date-picker/ngx';
@Component({
  selector: 'app-start-wizard',
  templateUrl: './start-wizard.page.html',
  styleUrls: ['./start-wizard.page.scss'],
})
export class StartWizardPage implements OnInit {

  @ViewChild('mySlider', { static: true }) slides: IonSlides;

  customAlertOptions: any = {
    header: 'Your Role',
    translucent: true,
    mode: 'ios'
  };

  customAlertOptionsBudget: any = {
    header: 'Estimate Budget',
    translucent: true,
    mode: 'ios'
  };
  customAlertOptionsGuest: any = {
    header: 'Guest count',
    translucent: true,
    mode: 'ios'
  };

  constructor(
    public datePicker: DatePicker
  ) { }

  ngOnInit() {
    this.slides.lockSwipes(true);
  }

  showDate() {
    this.datePicker.show({
      date: new Date(),
      mode: 'date',
      androidTheme: this.datePicker.ANDROID_THEMES.THEME_DEVICE_DEFAULT_LIGHT
    }).then(
      (date) => console.log('Got date: ', date),
      err => console.log('Error occurred while getting date: ', err)
    );
  }

  swipeNext() {
    this.slides.lockSwipes(false).then(() => {
      this.slides.slideNext().then(() => {
        this.slides.lockSwipes(true);
      }).catch(err => {
        this.slides.lockSwipes(true);
      });
    })
  }

}
