import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { StartWizardPage } from './start-wizard.page';

describe('StartWizardPage', () => {
  let component: StartWizardPage;
  let fixture: ComponentFixture<StartWizardPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StartWizardPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(StartWizardPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
