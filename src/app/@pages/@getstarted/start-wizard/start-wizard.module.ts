import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { StartWizardPageRoutingModule } from './start-wizard-routing.module';

import { StartWizardPage } from './start-wizard.page';
import { ComponentModule } from 'src/app/@component/component.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    StartWizardPageRoutingModule,
    ComponentModule
  ],
  declarations: [StartWizardPage]
})
export class StartWizardPageModule { }
