import { Component, OnInit, Input, OnChanges } from '@angular/core';

@Component({
  selector: 'nk-porgress-bar',
  templateUrl: './nk-porgress-bar.component.html',
  styleUrls: ['./nk-porgress-bar.component.scss'],
})
export class NkPorgressBarComponent implements OnInit, OnChanges {

  @Input() total: number = 2;
  @Input() fill: number = 1;
  totalPoint = [];
  constructor() { }

  ngOnInit() {
    // this.totalPoint = [];
    // for (var i = 0; i < this.total; i++) {
    //   this.totalPoint.push(i);
    // }
  }
  ngOnChanges() {
    console.log('fill', this.fill);
    console.log('total', this.total);
    this.totalPoint = [];
    for (var i = 0; i < this.total; i++) {
      this.totalPoint.push(i);
    }
    console.log('totalPoint', this.totalPoint);

  }

}
