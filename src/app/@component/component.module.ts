import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';
import { NkPorgressBarComponent } from './nk-porgress-bar/nk-porgress-bar.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ],
  declarations: [NkPorgressBarComponent],
  exports: [NkPorgressBarComponent]
})
export class ComponentModule {}
