import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  { path: '', redirectTo: 'login-home', pathMatch: 'full' },
  {
    path: 'login-home',
    loadChildren: () => import('./@pages/@login/login-home/login-home.module').then( m => m.LoginHomePageModule)
  },
  {
    path: 'login',
    loadChildren: () => import('./@pages/@login/login/login.module').then( m => m.LoginPageModule)
  },
  {
    path: 'sign-up',
    loadChildren: () => import('./@pages/@login/sign-up/sign-up.module').then( m => m.SignUpPageModule)
  },
  {
    path: 'forgot',
    loadChildren: () => import('./@pages/@login/forgot/forgot.module').then( m => m.ForgotPageModule)
  },  {
    path: 'getstarted',
    loadChildren: () => import('./@pages/@getstarted/getstarted/getstarted.module').then( m => m.GetstartedPageModule)
  },
  {
    path: 'start-wizard',
    loadChildren: () => import('./@pages/@getstarted/start-wizard/start-wizard.module').then( m => m.StartWizardPageModule)
  },


];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
 